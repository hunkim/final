(function () {
  'use strict';

  angular
    .module('core')
    .controller('CoreHomeMapController', CoreHomeMapController);

  CoreHomeMapController.$inject = ['$timeout', '$http', 'Colors', '$compile', '$scope', 'dateUtils', '$state'];
  function CoreHomeMapController($timeout, $http, Colors, $compile, $scope, dateUtils, $state) {
    var vm = this;
    var lastInfoWindow = undefined;
    $scope.serchKeyword=null;
    var icons = { 
      headquarter: {
        icon: './img/mapicons/if_building_35759.png'
      },
      laboratory: {
        icon: './img/mapicons/if_Retort_32545.png'
      },
      regionHeadquarter: {
        icon: './img/mapicons/if_buildings_101742.png'
      },
      trainingCenter: {
        icon: './img/mapicons/if_lyx_6537.png'
      }
    };
    
    activate();
    var array=[];
    var place,fixedMark;
    ////////////////
    //var initMapCenterPosition = {lat: 36.216207, lng:127.478371};
    function activate() {
      vm.addMarker = addMarker;
      


      // custom map style
      var MapStyles = [{ 'featureType': 'water', 'stylers': [{ 'visibility': 'on' }, { 'color': '#bdd1f9' }] }, { 'featureType': 'all', 'elementType': 'labels.text.fill', 'stylers': [{ 'color': '#334165' }] }, { featureType: 'landscape', stylers: [{ color: '#e9ebf1' }] }, { featureType: 'road.highway', elementType: 'geometry', stylers: [{ color: '#c5c6c6' }] }, { featureType: 'road.arterial', elementType: 'geometry', stylers: [{ color: '#fff' }] }, { featureType: 'road.local', elementType: 'geometry', stylers: [{ color: '#fff' }] }, { featureType: 'transit', elementType: 'geometry', stylers: [{ color: '#d8dbe0' }] }, { featureType: 'poi', elementType: 'geometry', stylers: [{ color: '#cfd5e0' }] }, { featureType: 'administrative', stylers: [{ visibility: 'on' }, { lightness: 33 }] }, { featureType: 'poi.park', elementType: 'labels', stylers: [{ visibility: 'on' }, { lightness: 20 }] }, { featureType: 'road', stylers: [{ color: '#d8dbe0', lightness: 20 }] }];
      vm.mapOptions1 = {
        zoom: 9,
        center: new google.maps.LatLng(36.216207, 127.478371),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        mapTypeControl: false,
        streetViewControl:false,
        rotateControl:false
        
      };
      function addSearchList(map) {
        var centerControlDiv = document.createElement('div');
        var centerControl = new CenterControl(centerControlDiv, map);

        centerControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);
      }
      function CenterControl(controlDiv, map) {
        var controlUI = document.createElement('div');
        controlDiv.appendChild(controlUI);
        var controlText = angular.element('<input id="searchPlace" type="text" placeholder="원하시는 항목을 입력해 주세요" class="form-control searchBox" ng-model="searchKeyword" auto-complete="autoCompleteOptions">');
        $compile(controlText)($scope);
        controlUI.appendChild(controlText[0]);
      }

      $http.get('/api/facilities').success(function (response) {
        for (var i = 0; i < response.length; i++) {
          $timeout(addMarker(vm.myMap1, response[i]));
        }
        $scope.autoCompleteOptions = {
          minimumChars: 2,
          dropdownWidth: '400px',
          containerCssClass: 'color-codes',
          selectedTextAttr: 'name',
          data: function (searchText) {
              searchText = searchText.toUpperCase();
              return _.filter(response, function (data) {
                  return data.name.includes(searchText);
              });
          },
          itemSelected: function (item) {
            place=item.item.name; 
            fixedMark=array.filter(function(item){ 
              return item.name===place;
            });
            vm.myMap1.setCenter({lat: item.item.latitude, lng: item.item.longitude}); //좌표정보로 마커
            var showInfo=new google.maps.Marker(fixedMark[0]); 
            showInfo.addListener("click",function(){
              infowindow.open(map,showInfo);//검색 후 정보창 오픈되도록 이벤트 설정       
            });
            google.maps.event.trigger(showInfo,"click"); //클릭이벤트로 정보창 오픈
            
          }
        };
        addSearchList(vm.myMap1);
        
      }).error(function (response) {
        alert(response.massage);
      });


      ///////////////
      function addMarker(map, data) {
        var iconType;
        switch (data.type) {
          case '지역본부':
            iconType = icons.regionHeadquarter.icon;
            break;
          case '연구소':
            iconType = icons.laboratory.icon;
            break;
          case '연수원':
            iconType = icons.trainingCenter.icon;
            break;
          case '사옥':
            iconType = icons.headquarter.icon;
            break;
        }
        var marker = new google.maps.Marker({
          map: map,
          position: { lat: data.latitude, lng: data.longitude },
          icon: iconType,
          name:data.name
        });

        array.push(marker);     


        marker.addListener('click',function () { //
          //이전창 닫기
          if (lastInfoWindow !== undefined) {
            lastInfoWindow.close();
          }
          var contentString = '<div id="">' +
            '<div id="">' +
            '</div>' +
            '<h4>' + data.name + '</h4>' +
            '<br>' +
            '시각화도구자리' +
            // '<br>마지막 업데이트: ' + dateUtils.yyyymmddhhmmss(data.updated) +
            '<br><a ng-click=changePrototypeState("' + data.SMD + '")>상세현황보기</a>' +
            '</div>';

          var infowindow = new google.maps.InfoWindow();
          $compile(contentString)($scope, function (compiled) {
            //Directive 미인식으로 초기화 한번 필요함
            if (lastInfoWindow === undefined) {
              google.maps.event.trigger(vm.myMap1, 'resize');
            }
            infowindow.setContent(compiled[0]);
            lastInfoWindow = infowindow;
            // setTimeout(undefined,2000);
            infowindow.open(map, marker);

            // $scope.$apply();
          });

        });

        return marker;
      }
    }
    $scope.changePrototypeState = function (SMD) {
      $state.go('app.prototype', { 'SMD': SMD  })
    };
  }
})();

