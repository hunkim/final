'use strict';

//TODO this should be Users service
angular.module('app.facility').factory('facilityInfo', ['$http','$q',
  function ($http,$q) {
    return {
      // getFacilityInfo: function (objectId,next) {
      getFacilityInfo: function (SMD, next) {  
        var deferred = $q.defer();
        var parameter = new Object();
        parameter.SMD = SMD;
        // parameter.id = objectId;
        $http.post('/api/facility', parameter).then(function (result) {
          deferred.resolve(result.data[0]);
          // deferred.resolve(result.data);
        });
        return deferred.promise;
      },
      getFacilityUsageInfo:function(equipmentId){
        var deferred = $q.defer();
        var parameter = new Object();
        parameter.EquipmentId=equipmentId;
        
        $http.post('/api/usagegetdata', parameter).then(function (result) {
          deferred.resolve(result.data);
        });
        return deferred.promise;
      }
    }
  }
]);
