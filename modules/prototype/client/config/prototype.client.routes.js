(function() {
    'use strict';

    angular
        .module('app.prototype')
        .config(appRoutes);
    appRoutes.$inject = ['$stateProvider', 'RouteHelpersProvider'];

    function appRoutes($stateProvider, helper) {

        $stateProvider
            .state('app.prototype', {
                // url:'/prototype/test/:objectId',
                url: '/prototype/test/:SMD',
                templateUrl: 'modules/prototype/client/views/prototype.client.view.html',
                controller : 'PrototypeDataController',
                resolve: helper.resolveFor('flot-chart', 'flot-chart-plugins', 'weather-icons','datatables','nvd3','c3','chart.js')
            })
            .state('app.prototype_v2', {
                url: '/prototype/v2',
                title: 'Prototype v2',
                templateUrl: 'modules/prototype/client/views/prototype_v2.client.view.html',
                controller: 'PrototypeV2Controller',
                controllerAs: 'proto2',
                resolve: helper.resolveFor('flot-chart', 'flot-chart-plugins')
            })
            .state('app.prototype_v3', {
                url: '/prototype/v3',
                title: 'Prototype v3',
                controller: 'PrototypeV3Controller',
                controllerAs: 'proto3',
                templateUrl: 'modules/prototype/client/views/prototype_v3.client.view.html',
                resolve: helper.resolveFor('flot-chart', 'flot-chart-plugins', 'vector-map', 'vector-map-maps')
            });

    }
})();