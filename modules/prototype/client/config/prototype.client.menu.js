(function() {
    'use strict';

    angular
        .module('app.prototype')
        .run(coreMenu);

    coreMenu.$inject = ['Menus'];
    function coreMenu(Menus){

        Menus.addMenuItem('sidebar', {
            title: 'Prototype',
            state: 'app.proto',
            type: 'dropdown',
            iconClass: 'icon-ghost',
            position: 12,
            roles: ['*']
        });

        Menus.addSubMenuItem('sidebar', 'app.proto', {
            title: 'Prototype v1',
            state: 'app.prototype'
        });
        Menus.addSubMenuItem('sidebar', 'app.proto', {
            title: 'Prototype v2',
            state: 'app.prototype_v2'
        });
        Menus.addSubMenuItem('sidebar', 'app.proto', {
            title: 'Prototype v3',
            state: 'app.prototype_v3'
        });

    }

})();