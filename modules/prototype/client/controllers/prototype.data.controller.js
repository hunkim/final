(function () {
    'use strict';

    angular
        .module('app.prototype')
        .controller('PrototypeDataController', PrototypeDataController);

        PrototypeDataController.$inject = ['$scope',  '$timeout', '$stateParams', '$http', 'dateUtils', 'facilityInfo','customerBaseLineUtils'];
    function PrototypeDataController($scope,  $timeout, $stateParams, $http, dateUtils, facilityInfo,customerBaseLineUtils) {
        var vm = this;
        var CBL_MODE={QUATER:3,HALF:6,HOUR:12};

        activate();

        function activate() {
            // facilityInfo.getFacilityInfo($stateParams.objectId).then(function(info){ 
            facilityInfo.getFacilityInfo($stateParams.SMD).then(function (info) { 
                facilityInfo.getFacilityUsageInfo(info.EquipmentId).then(function(usage){
                    customerBaseLineUtils.getCustomerBaseLineMax4Per5(info.EquipmentId,CBL_MODE.HOUR).then(function(cblDataHour){
                        customerBaseLineUtils.getCustomerBaseLineMax4Per5(info.EquipmentId,CBL_MODE.HALF).then(function(cblDataHalf){
                            customerBaseLineUtils.getCustomerBaseLineMax4Per5(info.EquipmentId,CBL_MODE.QUATER).then(function(cblDataQuater){
                                var data = new Object();
                                var date = new Date('2018-01-24');
                                data.cblDataQuater=cblDataQuater.data[0].value;
                                data.cblDataHalf=cblDataHalf.data[0].value;
                                data.cblDataHour=cblDataHour.data[0].value;
                                data.valueDataHour=[];
                                data.valueDataHalfHour=[];
                                data.valueDataQuaterHour=[];
                                for(var i=0;i<usage.length;i++){
                                    var index=usage[i].mmIndex-1;
                                    if (usage[i].mr_ymdhh === dateUtils.yyyymmdd(date)) {
                                        if (data.valueDataQuaterHour[parseInt(index / 3)] === undefined) {
                                            data.valueDataQuaterHour[parseInt(index / 3)] = 0;
                                        }
                                        data.valueDataQuaterHour[parseInt(index / 3)] +=usage[i].Value_5min;

                                        if (data.valueDataHalfHour[parseInt(index / 6)] === undefined) {
                                            data.valueDataHalfHour[parseInt(index / 6)] = 0;
                                        }
                                        data.valueDataHalfHour[parseInt(index / 6)] += usage[i].Value_5min;

                                        if (data.valueDataHour[parseInt(index / 12)] === undefined) {
                                            data.valueDataHour[parseInt(index / 12)] = 0;
                                        }
                                        data.valueDataHour[parseInt(index / 12)] += usage[i].Value_5min;
                                    }
                                }
                                data.usage=usage;
                                data.info=info;
                                $scope.$broadcast('facilityData',data);
                            });
                        });
                    });
                });
            });
            
        }
    }
})();